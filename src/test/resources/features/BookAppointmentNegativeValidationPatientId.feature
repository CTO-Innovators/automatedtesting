Feature: Book Appointment Negative Validation PatientId

@Test
  Scenario Outline: Book Appointment functional Negative validation Details Page Patien Id field
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "3" seconds.
  Then I select "<Department>" from "Department" select box
  Then I click on "AppointmentDate" AppointmentDate Date Picker.
  Then I select "<Date1>" date from date picker
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I click on "ANext" button.
  Then I wait for "2" seconds.
  Then I should see "<ErrorMsg>" on the page
  Then I wait for "5" seconds.

  
  
  Examples:
  |PatientId||Department||Date1||ErrorMsg|
  |||Dermatology||30||This field is required.|
  |$@#$@$#$||Dermatology||30||Invalid Patient|
  |aasff||Dermatology||30||Invalid Patient|