Feature: Registration UI validation 

@Test
  Scenario:  Register page UI validation
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I should see "Patient Registration" on the page
  Then I should see "First Name" on the page
  Then I should see "Last Name" on the page
  Then I should see "Contact Number" on the page
  Then I should see "Address" on the page
  Then I should see "Email" on the page
  Then I should see "Gender" on the page
  Then I should see "Date of Birth" on the page
  And I verified if the Colour of "Personal" Tab is blue for Register page
  Then I should see "Next" displayed on the page
  Then I wait for "3" seconds.
  