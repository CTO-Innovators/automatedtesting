Feature: Registration Negative Personal Page

@Test
  Scenario Outline: EZClinic Registration Negative validation for Personal page
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I enter "<FirstName>" into "FirstName" text field.
  Then I enter "<LastName>" into "LastName" text field.
  Then I enter "<ContactNumber>" into "ContactNumber" text field.
  Then I enter "<Address>" into "Address" text field.
  Then I enter "<Email>" into "Email" text field.
  Then I select "<Gender>" from "Gender" select box
  Then I enter "<DOB1>" into "DOB1" text field. 
  Then I click on "Address" Field.
  Then I wait for "2" seconds. 
  Then I click on "Next" button.
  Then I wait for "3" seconds.
  Then I should see "<errMsg>" on the page
  

 
 Examples:
 |FirstName||LastName||ContactNumber||Address||Email||Gender||YearDP||MonthDP||Date1||errMsg||DOB1|
 |||Rk||9620222088||Bangalore||rra@gmail.com||Female||1990||Mar||18||This field is required.||03/18/1990|
 |^$^%$^%||Rk||9620222088||chennai||rra@gmail.com||Female||1990||Mar||18||Please enter a valid First Name. Alphabet,' and - is allowed||03/18/1990|
 |Linda||||9620222088||Mumbai||rra@gmail.com||Female||1990||Mar||18||This field is required.||03/18/1990|
 |Jenifer||%^$%^$||9620222088||Hubli||rra@gmail.com||Female||1990||Mar||18||Please enter a valid Last Name. Alphabet,' and - is allowed||03/18/1990|
 |Sophia||Rk||9620222088||Mahadevapura||||Male||1990||Mar||18||This field is required.||03/18/1990|
 |Harishma||Rk||9620222088||Adugodi||%#%#&^%||Female||1990||Mar||18||Please enter a valid email address.||03/18/1990|
 |Alice||Rk||9620222088||Ejipura||rr@gm||Female||1990||Mar||18||Please enter a valid email address||03/18/1990|
 |Prabhu||Rk||||Bangalore||rrarr@gmail.com||Male||1990||Mar||18||This field is required.||03/18/1990|
 |Cassie||Rk||aaaaaaaaaa||Church road||rra@gmail.com||Female||1990||Mar||18||Only 10 digit numeric value is allowed||03/18/1990|
 |Elena||Rk||%$%$||Bangalore||rratt@gmail.com||Male||1990||Mar||18||Only 10 digit numeric value is allowed||03/18/1990|
 |caroline||Rk||9620222||Brigade||rrae@gmail.com||Male||1990||Mar||18||Only 10 digit numeric value is allowed||03/18/1990|
 |Bonnie||Rk||9620222088||||rraee@gmail.com||Female||1990||Mar||18||This field is required.||03/18/1990|
 