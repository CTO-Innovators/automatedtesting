Feature: Registration Previous and NEXT Button Check

@Test
  Scenario Outline: EZClinic Registration Previous and NEXT Button Check
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I enter "<FirstName>" into "FirstName" text field.
  Then I enter "<LastName>" into "LastName" text field.
  Then I enter "<ContactNumber>" into "ContactNumber" text field.
  Then I enter "<Address>" into "Address" text field.
  Then I enter "<Email>" into "Email" text field.
  Then I select "<Gender>" from "Gender" select box
  Then I click on "DOB" Field.
  Then I select "<YearDP>" from "YearDP" select box
  Then I select "<MonthDP>" from "MonthDP" select box
  Then I select "<Date1>" date from date picker
  Then I click on "Next" button.
  Then I wait for "3" seconds.
  Then I select "<CardCategory>" from "CardCategory" select box
  Then I click on "<CardType>" CardType
  Then I enter "<NameOnCard>" into "NameOnCard" text field.
  Then I enter "<CardNo>" into "CardNo" text field.
  Then I select "<CardExpYear>" from "CardExpYear" select box
  Then I select "<CardExpMonth>" from "CardExpMonth" select box
  Then I click on "PREVIOUS" button.
  Then I wait for "2" seconds.
  And I verified if the Colour of "Personal" Tab is blue for Register page
  And I should see the Colour of Tab "Payment" is not blue for Register page
  Then I wait for "3" seconds.
 
 Examples:
 |FirstName||LastName||ContactNumber||Address||Email||Gender||YearDP||MonthDP||Date1||CardCategory||CardType||NameOnCard||CardNo||CardExpYear||CardExpMonth|
 |Romi||Rk||9620222088||Bangalore||rra@gmail.com||Female||1990||Mar||19||Debit||Visa||Romi||99999999999988888||2028||Jan (01)|