Feature: Book Appointment Negative Validation Details Page Date Field

  @Test
  Scenario Outline: Book Appointment functional Negative validation Details Page Date field
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "3" seconds.
  Then I enter "<PatientId>" into "PatientId" field and click tab
  Then I select "<Department>" from "Department" select box
  Then I click on "ANext" button.
  Then I wait for "2" seconds.
  Then I should see "<ErrorMsg>" on the page
  Then I wait for "5" seconds.  
  
  Examples:
  |PatientId||Department||ErrorMsg|
  |1||Dermatology||This field is required.|