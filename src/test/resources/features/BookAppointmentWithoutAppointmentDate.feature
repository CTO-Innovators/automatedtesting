Feature: Book Appointment Without Appointment Date selected Negative validation

@Test
  Scenario Outline: Book Appointment Without Appointment Date selected Negative validation
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "3" seconds.
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I select "<Department>" from "Department" select box
  Then I click on "ANext" button.  
  Then I wait for "2" seconds.  
  Then I should see "This field is required." on the page
  Then I wait for "2" seconds.
    
  Examples:
  |PatientId||Department|
  |1||Dermatology|