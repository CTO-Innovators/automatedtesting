package com.selcukesfw.utilities;

import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.XMLConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {
	private static WebDriver driver;
    
    private DriverFactory(){}
     
    public static WebDriver getDriver(){
    	if(driver==null || (driver != null && driver.toString().contains("null"))){
            try{
            	XMLConfiguration config = new XMLConfiguration("configurations/object-config.xml");
            	String browserType = config.getString("browser");
            	if(browserType != null && browserType.equalsIgnoreCase("chrome"))
            	{
					System.setProperty("webdriver.chrome.driver", "C:\\Workspace\\EZClinic\\chromedriver.exe");
					driver = new ChromeDriver();
            	
            	}else{
            		driver = new FirefoxDriver();
            	}   
//            	driver = new HtmlUnitDriver(true);
        		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        		driver.manage().window().maximize();
            }catch(Exception e){
                throw new RuntimeException("Exception occured in creating driver object");
            }
    	}
        return driver;
    }
    
    public static void quit()
    {
    	if(!(driver==null || (driver != null && driver.toString().contains("null")))){
    		driver.quit();
    	}
    }
}
