Feature: Book Appointment UiValidation

@Test
  Scenario Outline:  Book Appointment UI validation Positive 
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "4" seconds.
  Then I should see "Book Appointment" on the page
  Then I verified if the Colour of "details" Tab is blue for Book Appointment Page
  Then I should see "Appointment" on the page
  And I should see "Patient ID" on the page
  Then I should see "Department" on the page
  Then I should see "Date" on the page
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I click on "AppointmentDate" AppointmentDate Date Picker.
  Then I select "<Date1>" date from date picker
  Then I select "<Department>" from "Department" select box
  Then I click on "ANext" button.
  Then I verified if the Colour of "appointment" Tab is blue for Book Appointment Page
  Then I wait for "5" seconds.
  Then I should see "TimeSlotUI" displayed on the page
  Then I wait for "2" seconds.
  Then I should see "PreviousNNEXTButton" displayed on the page
  Then I wait for "5" seconds.
  
  Examples:
  |PatientId||Department||Date1|
  |1||Dermatology||30|