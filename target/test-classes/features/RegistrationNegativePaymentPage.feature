Feature: Registration Negative Payment Page

@Test
  Scenario Outline: EZClinic Registration Negative validation payment page
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I enter "<FirstName>" into "FirstName" text field.
  Then I enter "<LastName>" into "LastName" text field.
  Then I enter "<ContactNumber>" into "ContactNumber" text field.
  Then I enter "<Address>" into "Address" text field.
  Then I enter "<Email>" into "Email" text field.
  Then I select "<Gender>" from "Gender" select box
  Then I enter "<DOB1>" into "DOB1" text field. 
  Then I click on "Address" Field.
  Then I wait for "2" seconds. 
  Then I click on "Next" button.
  Then I select "<CardCategory>" from "CardCategory" select box
  Then I click on "<CardType>" CardType
  Then I enter "<NameOnCard>" into "NameOnCard" text field.
  Then I wait for "3" seconds.
  Then I enter "<CardNo>" into "CardNo" text field.
  Then I select "<CardExpYear>" from "CardExpYear" select box
  Then I select "<CardExpMonth>" from "CardExpMonth" select box
  Then I click on "RegFINISH" button.
  Then I wait for "3" seconds.
  Then I should see "<errMsg1>" on the page
  Then I wait for "3" seconds.
  Then I should not see "Registration is Successful !!" on the page
   Then I wait for "2" seconds.
 
 Examples:
 |FirstName||LastName||ContactNumber||Address||Email||Gender||YearDP||MonthDP||Date1||CardCategory||CardType||NameOnCard||CardNo||CardExpYear||CardExpMonth||errMsg1||DOB1|
 |Romi||Rk||9620222088||chennai||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||||9999999999990000||2028||Jan (01)||This field is required.||03/18/1990|
 |Linda||Rkeeee||9620222033||mumbai||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||%$#%$^%$||9999999999990000||2028||Feb (02)||Please enter valid name. Alphabet,' and - is allowed||03/18/1990|
 |jenifer||Rk||9620222066||Delhi||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||Romi||||2028||Mar (03)||This field is required.||03/18/1990|
 |Harry||Rk||9620222099||Kolkata||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||Romi||%$#%$#%||2028||Jan (01)||Only 16 digit numeric value is allowed||03/18/1990|
 |Harry||Rk||9620222099||Kolkata||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||Romi||aaaaaaaaaaaa||2028||Feb (02)||Only 16 digit numeric value is allowed||03/18/1990|
 |Harry||Rk||9620222099||Kolkata||rra@gmail.com||Female||1990||Mar||18||Debit||MasterCard||Romi||aaaaaa23447||2028||Feb (02)||Only 16 digit numeric value is allowed||03/18/1990|