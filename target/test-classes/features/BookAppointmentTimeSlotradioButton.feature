Feature: Book Appointment Negative Validation TimeSlot radio Button Toggle check

  @Test
  Scenario Outline:  Book Appointment TimeSlot radio Button Toggle check
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "5" seconds.
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I click on "AppointmentDate" AppointmentDate Date Picker.
  Then I select "<Date1>" date from date picker
  Then I select "<Department>" from "Department" select box
  Then I click on "ANext" button.
  Then I select "<TimeSlot>" TimeSlot
  Then I wait for "5" seconds.
  Then I should see TimeSlot "Slot1" is checked
  Then I select "2" TimeSlot
  Then I wait for "5" seconds.
  Then I should see TimeSlot "Slot2" is checked
  Then I should see TimeSlot "Slot1" is unchecked
  
  
    Examples:
  |PatientId||Department||Date1||Doctor||TimeSlot|
  |3||Dermatology||30||swapna patel||1|
  