Feature: Registration without patient DOB Negative validation

@Test
  Scenario Outline: Registration without patient DOB  Negative validation
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I enter "<FirstName>" into "FirstName" text field.
  Then I enter "<LastName>" into "LastName" text field.
  Then I enter "<ContactNumber>" into "ContactNumber" text field.
  Then I enter "<Address>" into "Address" text field.
  Then I enter "<Email>" into "Email" text field.
  Then I select "<Gender>" from "Gender" select box
  Then I enter "<DOB1>" into "DOB1" text field. 
  Then I click on "Address" Field.
  Then I wait for "2" seconds. 
  Then I click on "Next" button.
  Then I wait for "3" seconds.
  Then I should see "<errMsg>" on the page
  

 
 Examples:
 |FirstName||LastName||ContactNumber||Address||Email||Gender||YearDP||MonthDP||Date1||errMsg||DOB1|
 |Romi||Rk||9620222088||Bangalore||rra@gmail.com||Female||1990||Mar||18||This field is required.|||