Feature: Book Appointment For Past Date Negative validation

@Test
  Scenario Outline: Book Appointment For Past Date Negative validation
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "3" seconds.
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I enter "11/11/1990" into "BookAppointmentDate" text field.
  Then I select "<Department>" from "Department" select box
  Then I wait for "3" seconds.  
  Then I should see "Only future Date is allowed" on the page
  Then I wait for "2" seconds.   
  Examples:
  |PatientId||Department|
  |1||Dermatology|