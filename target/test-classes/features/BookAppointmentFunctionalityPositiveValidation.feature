Feature: Book Appointment Functionality Positive Validation

@Test
  Scenario Outline: Book Appointment Positive Validation
  
  Given I open "URL" URL in the browser
  Then I wait for "2" seconds.
  Then I click on "BookAppointmentTab" button.
  Then I wait for "3" seconds.
  Then I enter "<PatientId>" into "PatientId" text field.
  Then I click on "AppointmentDate" AppointmentDate Date Picker.
  Then I select "<Date1>" date from date picker
  Then I select "<Department>" from "Department" select box
  Then I wait for "50" seconds.  
  Then I click on "ANext" button.
  Then I select "<TimeSlot>" TimeSlot
  Then I wait for "5" seconds.
  Then I should see TimeSlot "Slot1" is checked 
  Then I click on "AFINISH" button.
  Then I wait for "10" seconds.
  Then I should see "Appointment Booking is Successful" on the page
  Then I should see "<PatientId>" on the page
  Then I should see "<Doctor>" on the page
  Then I should see "<Department>" on the page
  Then I should see "<Date1>" on the page
  Then I wait for "2" seconds.
   

  
    Examples:
  |PatientId||Department||Date1||Doctor||TimeSlot|
  |1||Dermatology||22||swapna patel||1|